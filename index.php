<?php

// Get dice values
$Dice_A = rand(1, 6);
$Dice_B = rand(1, 6);

echo "
<html>
    <head>
        <title>Dice Roller</title>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>

<style>
    body {
        padding: 0;
        margin: 0;
        color: #fff;
        background-color: #000;
    }

    div.dice-container {
        width: 100%;
        text-align: center;
        margin: 0 auto 0 auto;
    }

    img.dice {
        padding: 10px;
        height: 150px;
        
    }
</style>

<body>
<a href='index.php' style='height: 100%; width:100%; position: fixed;'></a>

<div class='dice-container'>
<h1>Backgammon Dice</h1>
<img class='dice' src='img/$Dice_A.svg' alt='Dice Roll: $Dice_A'>
<img class='dice' src='img/$Dice_B.svg' alt='Dice Roll: $Dice_B'>
</div>

</body>
</html>
";
    
// EOF